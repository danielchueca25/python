import sys

import psycopg2


def data_input(text):
    while True:
        option = input(text)
        if option.isdigit():
            inp = int(option)
            if inp > len(rows)+1:
                print("No es una opcion valida")
            else:
                return option
        else:
            print("No es un numero, introduce un numero ")



try:
    # Exercici 1
    conn = psycopg2.connect("dbname='Bddemo' user='odoo' host='172.17.0.1' password='odoo'")
    cur = conn.cursor()
    print("\n\t\t\t", "Ejercicio 1:")
    cur.execute("SELECT * FROM res_users WHERE id>%s", (1,))
    rows = cur.fetchall()
    for row in rows:
        print(row)
    # Exercici 2
    print("\n\t\t\t", "Ejercicio 2:")
    cur.execute("DROP TABLE IF EXISTS taulatest")
    cur.execute("CREATE TABLE taulatest(Id int PRIMARY KEY, data VARCHAR(10))")

    cur.execute("INSERT INTO taulatest VALUES (1,'padata1')")
    cur.execute("INSERT INTO taulatest VALUES (2,'padata2')")
    cur.execute("INSERT INTO taulatest VALUES (3,'padata3')")
    cur.execute("INSERT INTO taulatest VALUES (4,'padata4')")
    cur.execute("INSERT INTO taulatest VALUES (5,'padata5')")
    conn.commit()
    # Exercici 3
    print("\n\t\t\t", "Ejercicio 3:")
    cur.execute("SELECT Id FROM taulatest")
    rows2 = cur.fetchall()
    for i in rows2:
        print(i)
    var = data_input("De que registro quieres los datos?")
    cur.execute("SELECT * FROM taulatest WHERE id=%s", (var[-1]))
    field_rows = cur.fetchall()
    for i in field_rows:
        print(i)
    # Exercici 4
    print("\n\t\t\t", "Ejercicio 4:")
    cur.execute("SELECT Id FROM taulatest")
    rows2 = cur.fetchall()
    for i in rows2:
        print(i)

    # opcion escogida
    var2 = data_input("De que registro quieres actualizar los datos?")

    text = input("Escribe el nuevo dato: ")
    cur.execute("UPDATE taulatest SER data=%s WHERE Id=%s", (text, var2))
    conn.commit()
    print("Filas modificadas: {0}".format(cur.rowcount))
    cur.execute("SELECT * FROM taulatest WHERE id=%s", (var2[-1]))
    field_rows2 = cur.fetchall()
    for i in field_rows2:
        print(i)


except (Exception, psycopg2.DatabaseError) as error:
    if conn is not None:
    conn.rollback()
    print("--Error {0}".format(error))
    sys.exit(1)

finally:
    if conn is not None:
        conn.close()
