import psycopg2

PSQL_HOST = "172.17.0.1"
PSQL_PORT = "5432"
PSQL_USER = "odoo"
PSQL_PASS = "odoo"
PSQL_DB = "postgres"

connection_adress = """
host=%s port=%s user=%s password=%s dbname=%s
""" % (PSQL_HOST,PSQL_PORT,PSQL_USER,PSQL_PASS,PSQL_DB)

connection = psycopg2.connect(connection_adress)

cursor = connection.cursor()

SQL = "SELECT * FROM anime;"
cursor.execute(SQL)

all_values = cursor.fetchall()

cursor.close()
connection.close()
print ("Get values: ",all_values)