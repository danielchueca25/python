from client import Client


class Llibreta:
    lista_clientes: list[Client]
    id_cliente: int

    def id_next(self) -> int:
        self.id_cliente = self.id_cliente + 1
        return self.id_cliente

    def __init__(self) -> None:
        self.lista_clientes = []
        self.id_cliente = 0

    def get_lista_clientes(self) -> list[Client]:
        return self.lista_clientes
        # for cl in self.lista_clientes:
            # print(cl.__str__())

    def anyadir_client(self, nom, apellido, tel, correo, direccion, ciudad) -> None:
        cliente = Client(self.id_next(), nom, apellido, tel, correo, direccion, ciudad)
        self.lista_clientes.append(cliente)

    def eliminar_client(self, num) -> None:
        for c in self.lista_clientes:
            if c.id_cl == num:
                self.lista_clientes.remove(c)

    def buscar_id_client(self, num) -> Client:
        for c in self.lista_clientes:
            if c.id_cl == num:
                return c.__str__()

    def buscar_nom_client(self, nom) -> Client:
        for c in self.lista_clientes:
            if c.nom == nom:
                return c

    def buscar_apellido_client(self, apellido) -> Client:
        for c in self.lista_clientes:
            if c.apellido == apellido:
                return c.__str__()


# l1.anyadir_client("Juan", "Tenorio", 666, "tenorio.juan@gmail.com", "c/ tenebrosa n6", "Sims")


