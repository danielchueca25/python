class Client:
    id_cl: int
    nom: str
    apellido: str
    tel: int
    correo: str
    direccion: str
    ciudad: str

    def __init__(self, id_cl, nom, apellido, tel, correo, direccion, ciudad) -> None:
        self.id_cl = id_cl
        self.nom = nom
        self.apellido = apellido
        self.tel = tel
        self.correo = correo
        self.direccion = direccion
        self.ciudad = ciudad

    def __str__(self) -> str:
        return self.id_cl, self.nom, self.apellido, self.tel, self.direccion, self.ciudad

