from client import Client
from llibreta import Llibreta as Lib

l = Lib()
l.anyadir_client("Juan", "Tenorio", 666, "tenorio.juan@gmail.com", "c/ tenebrosa n6", "Sims")
l.anyadir_client("h", "Tenorio", 666, "tenorio.juan@gmail.com", "c/ tenebrosa n6", "Sims")
l.anyadir_client("a", "Tenorio", 666, "tenorio.juan@gmail.com", "c/ tenebrosa n6", "Sims")
l.anyadir_client("j", "Tenorio", 666, "tenorio.juan@gmail.com", "c/ tenebrosa n6", "Sims")


def principal():
    print("\n\nMENU PRNCIPAL")
    print("======================================")
    print("Selecciona una opcion y dale a intro")
    print("======================================")
    print("\t1. Añadir cliente")
    print("\t2. Eliminar cliente")
    print("\t3. Consultar cliente")
    print("\t4. Modificar un campo de un cliente (*)")
    print("\t5. Salir")

    op = input("\nEntra una opcion: ")
    if op.isdigit():
        op = int(op)

        if 1 == op:
            nom = input("Introduce un nombre: ")
            apellido = input("Introduce un apellido: ")
            tel = input("Introduce un telefono: ")
            tel = isInt(tel)
            correo = input("Introduce un correo: ")
            direccion = input("Introduce una direccion: ")
            ciudad = input("Introduce una ciudad: ")

            l.anyadir_client(nom, apellido, tel, correo, direccion, ciudad)
            principal()

        elif 2 == op:
            for cl in l.get_lista_clientes():
                print(cl.__str__())

            num = input("Introduce el identificador del cliente que quieres eliminiar")
            num = isInt(num)

            l.eliminar_client(num)
            principal()

        elif 3 == op:
            consulta()

        elif 4 == op:
            nombre = input("Introduce el nombre de un cliente: ")
            correcto = False
            while (correcto == False):
                if l.buscar_nom_client(nombre) is not None:
                    campo = input("Que campo quieres modificar:"
                                  "\n\t1. Nombre"
                                  "\n\t2. Apellido"
                                  "\n\t3. Telefono"
                                  "\n\t4. Correo"
                                  "\n\t5. Direccion"
                                  "\n\t6. Ciudad"
                                  "\nEntra el numero: ")
                    campo = isInt(campo)

                    if campo == 1:
                        l.buscar_nom_client(nombre).nom = input("Introduce un nombre nuevo: ")
                        correcto = True
                    elif campo == 2:
                        l.buscar_nom_client(nombre).apellido = input("Introduce un apellido nuevo: ")
                        correcto = True
                    elif campo == 3:
                        num = input("Introduce un telefono nuevo: ")
                        num = isInt(num)
                        l.buscar_nom_client(nombre).tel = num
                        correcto = True
                    elif campo == 4:
                        l.buscar_nom_client(nombre).correo = input("Introduce un correo nuevo: ")
                        correcto = True
                    elif campo == 5:
                        l.buscar_nom_client(nombre).direccion = input("Introduce un direccion nuevo: ")
                        correcto = True
                    elif campo == 6:
                        l.buscar_nom_client(nombre).ciudad = input("Introduce un ciudad nuevo: ")
                        correcto = True
                    else:
                        print("No has escogido un campo valido")
                else:
                    print("El cliente no existe")
                    correcto = True

            print(l.buscar_nom_client(nombre).__str__())
            principal()

        elif 5 == op:
            print("ADIOS")

        else:
            print("Entra una numero entre 1 y 5")
            principal()
    else:
        print("No has escrito un numero")
        principal()


def consulta():
    print("\n\nMENU CONSULTA")
    print("======================================")
    print("Selecciona una opcion y dale a intro")
    print("======================================")
    print("\t1. Busca un cliente por Identificador")
    print("\t2. Busca un cliente por Nombre")
    print("\t3. Busca un cliente por Apellido")
    print("\t4. Lista todos los clientes")
    print("\t5. Lista todos los clientes por Nombre (*)")
    print("\t6. Atras")

    op = input("\nEntra una opcion: ")
    if op.isdigit():
        op = int(op)

        if 1 == op:
            num = input("Introduce un numero entero: ")
            num = isInt(num)
            print(l.buscar_id_client(num))
            consulta()

        elif 2 == op:
            nombre = input("Introduce el nombre de un cliente: ")
            print(l.buscar_nom_client(nombre).__str__())
            consulta()

        elif 3 == op:
            apellido = input("Introduce el apellido de un cliente: ")
            print(l.buscar_apellido_client(apellido))
            consulta()

        elif 4 == op:
            for cl in l.get_lista_clientes():
                print(cl.__str__())
            consulta()

        elif 5 == op:
            listSort = sorted(l.get_lista_clientes(), key=lambda client: client.nom)
            for c in listSort:
                print(c.__str__())
            consulta()

        elif 6 == op:
            print("ATRAS")
            principal()

        else:
            print("Entra una numero entre 1 y 5")
            consulta()
    else:
        print("No has escrito un numero")
        consulta()


def isInt(op) -> int:
    isNum = False
    while (isNum == False):
        if op.isdigit():
            op = int(op)
            isNum = True
            return op
        else:
            op = input("Introduce un numero entero: ")
