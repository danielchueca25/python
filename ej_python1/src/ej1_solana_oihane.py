import math

# ******************************************* TIPOS BASICOS ******************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 1
print("\tEjercicio 1")
print("Dame dos numeros y calculare su media aritmetica: ")
num1 = int(input("Introduce el primer numero: "))
num2 = int(input("Introduce el segundo numero: "))

print("La medida aritmetica es: ", (num1 + num2)/2 )


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 2
print("\n\n\tEjercicio 2")
print("Dame dos numeros (en pies y en pulgadas) y calculare su medida en cm: ")
pies = float(input("Introduce los pies: "))
pulgadas = float(input("Introduce las pulgadas: "))

op1 = (pies * 12) * 2.54
op2 = pulgadas * 2.54

print("De pies a cm: ", op1)
print("De pulgadas a cm: ", op2)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 3
# print("\n\n\tEjercicio 3")
print("Dame un numeros en Celcius y calculare su temperatura en Fahrenheit: ")
celsius = float(input("Introduce la temperatura en grados Celsius: "))

farh = (celsius * 1.8) + 32

print("Temperatura en graus Fahrenheit: ", farh, "F")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 4
# print("\n\n\tEjercicio 4")
print("Dame un numero en segundos y calculare su tiempo en minutos y segundos: ")
seg = int(input("Introduce los segundos: "))

min = int(seg/60)
seg -= min*60

print("El tempo se queda en ", min, "\' y ", seg, "\"")



# ********************************* MULTIPLICACIONES DE COLECCIONES **********************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 5
print("\n\n\tEjercicio 5")
list=['A', 'E', 'I', 'O', 'U']
print(list)
num = int(input("Escribe que posicion quieres modificar de la lista anterior (1 a 5): "))
val = str(input("Escribe con que quieres remplazarlo: "))

list[num-1] = val
print(list)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 6
print("\n\n\tEjercicio 6")
mult7 = [x * 7 for x in range(11)]
print("Lista de multiples de 7 hasta el decimo numero (inclusive el 0)")
print(mult7)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 7
print("\n\n\tEjercicio 7")
dictAlumn = {
    "Oihane": 9,
    "Judith": 8,
    "Dunia": 6,
    "Aimar": 10,
    "Joseba": 7
}

print(dictAlumn)
alumn = int(input("Escribe que posicion para saber de quien quieres la nota del diccionario anterior (1 a 5): "))

listKey = list(dictAlumn.keys())
listVal = list(dictAlumn.values())

print("La nota de", listKey[alumn-1], "es", listVal[alumn-1])


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 8
print("\n\n\tEjercicio 8")
listA = ['Cyan','magenta', 'yellow', 'RoSa', 'Lila']
listB = ['Morado','rosa', 'NarNaja', 'Cyan', 'MageNTa']

print("Insercion:")
print(set(listA).intersection(listB))

print("Diferencia si 1ra no 2da:")
print(set(listA).difference(listB))

print("Diferencia si 2da no 1ra:")
print(set(listB).difference(listA))

print("Union:")
print(set(listA).union(listB))



# ************************************* SENTENCIAS CONDICIONALES *************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 9
print("\n\n\tEjercicio 9")
print("Dame dos numeros y te dire si es entero o decimal su division: ")
div1 = int(input("Introduce el primer numero: "))
div2 = int(input("Introduce el segundo numero: "))

div = div1/div2

if div % 2 == 0:
    print("La division es exacta")
else:
    print("La division es decimal")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 10
# print("\n\n\tEjercicio 10")
menuOp = int(input("Dame si quieres calcular el area de un ciruclo (1) o de un triangulo (2): "))

if menuOp == 1:
    base = float(input("Introduce la base: "))
    altura = float(input("Introduce la altura: "))
    area = (base * altura) / 2
    print("La area es:", area)
elif menuOp == 2:
    radio = float(input("Introduce el radio: "))
    area = math.pi * math.pow(radio, 2)
    print("La area es:", area)
else:
    print("No sabes escribir el numero que te pido")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 11
print("\n\n\tEjercicio 11")
dictEuro = {
    "Billete de 50": 0,
    "Billete de 20": 0,
    "Billete de 10": 0,
    "Billete de 5": 0,
    "Moneda de 2": 0,
    "Moneda de 1": 0
}

print("Dame un numero en euros y calculare cuantos billetes tendras: ")
euros = int(input("Introduce el dinero: "))

if euros >= 50:
    varX = int(euros/50)
    dictEuro["Billete de 50"] = varX
    euros -= varX*50
if euros >= 20:
    varX = int(euros/20)
    dictEuro["Billete de 20"] = varX
    euros -= varX*20
if euros >= 10:
    varX = int(euros/10)
    dictEuro["Billete de 10"] = varX
    euros -= varX*10
if euros >= 5:
    varX = int(euros/5)
    dictEuro["Billete de 5"] = varX
    euros -= varX*5
if euros >= 2:
    varX = int(euros/2)
    dictEuro["Moneda de 2"] = varX
    euros -= varX*2
if euros >= 1:
    varX = int(euros/1)
    dictEuro["Moneda de 1"] = varX
    euros -= varX*1

print(dictEuro)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 12
print("\n\n\tEjercicio 12")
dia = int(input("Introduce el dia: "))
mes = int(input("Introduce el mes: "))
anyo = int(input("Introduce el anyo: "))

if mes in {1,3,5,7,8,10,12}:
    if dia > 31:
        print("Fecha incorrecta, el dia es mas grande de 31")
    else:
        print("Tu fecha es: ", dia, "/", mes, "/", anyo)

elif mes in {4,6,9,11}:
    if dia > 30:
        print("Fecha incorrecta, el dia es mas grande de 30")
    else:
        print("Tu fecha es: ", dia, "/", mes, "/", anyo)

elif mes==2:
    if dia > 29:
        print("Fecha incorrecta, el dia es mas grande de 29")
    else:
        if anyo%400==0 and dia<=29:
            print("Tu fecha es: ", dia, "/", mes, "/", anyo)
        elif anyo%4==0 and anyo%100!=0 and dia<=29:
            print("Tu fecha es: ", dia, "/", mes, "/", anyo)
        else:
            print("Fecha incorrecta, no has tenido en cuenta si es un año de traspaso")

else:
    print("Fecha incorrecta, el mes es mas grande de 12 o mas pequeño que 0")



# ******************************************* SOBRE BUCLES *******************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 13
print("\n\n\tEjercicio 13")
print("Dame dos numeros y te dire la suma de todos los enteros desde el primero al segundo: ")
num1 = int(input("Introduce el primer numero: "))
num2 = int(input("Introduce el segundo numero: "))

if num1>num2:
    print("El primer numero no puede ser mayor al segundo")
else:
    result=0
    for x in range(num1+1, num2):
        result = result + x
    print("La suma de los numeros entre", num1, "y", num2, "es", result)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 14
print("\n\n\tEjercicio 14")
print("Dame un numero mayor que 0 y te dire su factorial: ")
facNum = int(input("Introduce el numero: "))

if facNum <= 0:
    print("El numero no puede ser menor que 0")
else:
    result=1
    for x in range(2, facNum+1):
        result = result * x
    print(result)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 15
print("\n\n\tEjercicio 15")
print("Los 100 primeros numeros multiples de 7: ")

listMult7=[]
for x in range(0,100):
    if (x%7==0):
        listMult7.append(x)
print(listMult7)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 16
print("\n\n\tEjercicio 16")
print("Vamos a crear una lista de palabras: ")
listLength = int(input("Introduce el numero de palabras que quieres introducir: "))

listPalab = []
for x in range(0, listLength):
    palabra = str(input("Introduce una palabra: "))
    listPalab.append(palabra)
print(listPalab)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 17
print("\n\n\tEjercicio 17")
print("Dadas dos listas de palabras elimina las ocurrencias de la segunda lista en la primera: ")

listCurel1 = ["Spiderman", "Padre sp", "Madre sp", "Tio Ben", "Tia May", "Batman", "Padre b", "Madre b"]
listCurel2 = ["Tio Ben", "Tia May", "Padre sp", "Madre sp", "Padre b", "Madre b"]
print("Es la lista de familiares: ", listCurel1)
print("Es la lista de muertos: ", listCurel2)

for x in listCurel2:
    if x in listCurel1:
        listCurel1.remove(x)

print ("El resultado de los no muertos: ", listCurel1)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 18
print("\n\n\tEjercicio 18")
print("Dame un numero y te dire si es primo: ")
numPrimo = int(input("Escribe el numero: "))

isPrime = True
numTemp = 0
for x in range(2, numPrimo):
    if numPrimo % x == 0:
        isPrime = False
        numTemp = x
        break

if not isPrime:
    print("No es primo,", numTemp, "es un divisor suyo")
else:
    print("Es primo")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 19
print("\n\n\tEjercicio 19")
print("Dame un numero de tres cifras y te dire si es narcicista: ")
numNarc = int(input("Escribe el numero: "))

tempStr = str(numNarc)
tempNum = len(tempStr)
sum = 0
for x in tempStr:
    sum += math.pow(int(x), tempNum)

if sum == numNarc:
    print("El numero es narcicista")
else:
    print("El numero NO es narcicista")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 20
print("\n\n\tEjercicio 20")
print("Dame dos numeros y te dire los enteros entre ellos en orden creciente: ")
num1 = int(input("Escribe el priemer numero: "))
num2 = int(input("Escribe el segundo numero: "))

if num1>=num2:
    print("El primer numero no puede ser mayor al segundo")
else:
    listEntero=[]
    for x in range(num1, num2+1):
        listEntero.append(x)

    listSorted = sorted(listEntero, reverse=False)
    print(listSorted)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 21
print("\n\n\tEjercicio 21")
print("Dame dos numeros y te dire los enteros entre ellos en orden decreciente: ")
num1 = int(input("Escribe el priemer numero: "))
num2 = int(input("Escribe el segundo numero: "))

if num1>=num2:
    print("El primer numero no puede ser mayor al segundo")
else:
    listEntero=[]
    for x in range(num1, num2+1):
        listEntero.append(x)

    listSorted = sorted(listEntero, reverse=True)
    print(listSorted)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 22
print("\n\n\tEjercicio 22")
print("Dame dos numeros y te dire los enteros entre ellos pero si raiz cuadrada en orden creciente: ")
num1 = int(input("Escribe el priemer numero: "))
num2 = int(input("Escribe el segundo numero: "))

if num1>=num2:
    print("El primer numero no puede ser mayor al segundo")
else:
    listEntero=[]
    for x in range(num1, num2+1):
        listEntero.append(math.sqrt(x))

    listSorted = sorted(listEntero, reverse=False)
    print(listSorted)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 23
print("\n\n\tEjercicio 23")
print("Dame dos numeros y te dire los enteros entre ellos pero solo los pares en orden creciente: ")
num1 = int(input("Escribe el priemer numero: "))
num2 = int(input("Escribe el segundo numero: "))

if num1>=num2:
    print("El primer numero no puede ser mayor al segundo")
else:
    listEntero=[]
    for x in range(num1, num2+1):
        if x % 2 == 0:
            listEntero.append(x)

    listSorted = sorted(listEntero, reverse=False)
    print(listSorted)



# *********************************** SOBRE BUCLES Y COMPREHESION ************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 24
print("\n\n\tEjercicio 24")
print("Los 10 primeros multiples de 2: ")

listMult2 = []
for x in range(1, 11):
    listMult2.append(x * 2)
print(listMult2)


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 25
print("\n\n\tEjercicio 25")
print("Los n primeros multiples de m: ")
valN = int(input("Dame el valor de n: "))
valM = int(input("Dame el valor de m: "))

listMultM = []
for x in range(1, valN+1):
    listMultM.append(x * valM)
print(listMultM)



# ********************************************* CADENAS **********************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 26
print("\n\n\tEjercicio 26")
print("Dame una palabra y te dire si es alfabetica")
palabra = str(input("Introduce una parabra: "))

if list(palabra) == sorted(palabra):
    print("La palabra es alfabètica")
else:
    print("La palabra NO es alfabetica")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 27
print("\n\n\tEjercicio 27")
print("Dame una palabra y te dire si es alfabetica")
palabra = str(input("Introduce una parabra: "))

if str(palabra) == str(palabra)[::-1]:
    print("La palabra es palindroma")
else:
    print("La palabra NO es palindroma")


# ----------------------------------------------------------------------------------------------------
# EJERCICIO 28
print("\n\n\tEjercicio 28")
print("Dame un DNI y te dire si es correcta la letra")
dni = input("Introduce el dni: ")

letra = "TRWAGMYFPDXBNJZSQVHLCKE"
div = int(dni[0:8]) % 23
if letra[div]==dni[8]:
    print('El DNI es correcto')
else:
    print('El DNI es incorrecto')


# ********************************************* FICHEROS *********************************************
# ----------------------------------------------------------------------------------------------------
# EJERCICIO 29
print("\n\n\tEjercicio 29")
print("Leeme un archivo y te escribo las palabras no repetidas (case insensitive)")
file = open("hola.txt", "r")

line = file.read().lower().split()
line = list(dict.fromkeys(line))
line.sort()
print(line)